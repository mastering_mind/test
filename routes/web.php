<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarController;

Route::middleware('auth:api')->get('/cars/available', [CarController::class, 'getAvailableCars']);
