<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function getAvailableCars(Request $request)
    {
        $currentUser = $request->user();
        $scheduledTime = $request->input('scheduled_time');
        $model = $request->input('model');
        $comfortCategory = $request->input('comfort_category');

        $query = Car::whereDoesntHave('bookings', function ($query) use ($scheduledTime) {
            $query->where('scheduled_time', $scheduledTime);
        });

        if ($model) {
            $query->where('model', $model);
        }

        if ($comfortCategory) {
            $query->whereHas('comfortCategory', function ($query) use ($comfortCategory) {
                $query->where('name', $comfortCategory);
            });
        }

        $availableCars = $query->get();

        return response()->json($availableCars);
    }
}
